# laravel-react-now-pizza
Now Pizza - Prototype for ordering pizza

## Technologies

- (Laravel v7)[https://laravel.com/docs/7.x]

## Instalation

`composer install`

## Deployment

- [Deploy Laravel application with CI/CD and Envoy](https://medium.com/@koteghanshyam/deploy-laravel-application-with-ci-cd-and-envoy-3b66eaec85f6)
- [Dockerfile](https://github.com/lorisleiva/laravel-docker/blob/master/7.4/Dockerfile)

## Features

- Menu for ordering pizzas.
- Simple shopping cart. (Preview and Page)
- Checkout page: shipping address and contact information.
- Prices in Euros and Dollars.
- Place orders and list.

## Unit tests

- List Products
- List Orders

### TODO:

- Create, Update, Delete Products.
- Create, Update, Delete Orders.
- List, Create, Update, Delete HistoryExchangeRates.
- List, Create, Update, Delete Users.
- List, Create, Update, Delete AddressData.
- List, Create, Update, Delete ContactData.
- Register and Login.

## Deployment information

- IP: 198.74.53.105
- Domain: [nowpizza.xyz](http://nowpizza.xyz)
- Server information:
  -- Ubuntu 20.04
	-- Nginx, PHP, MySQL

## Wakatime (hours development)

![Wakatime hours](http://nowpizza.xyz/img/wakatime.png)

## Developer

- Ramon Serrano <rameduardserrano@gmail.com>
