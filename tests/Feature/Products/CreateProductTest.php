<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateProductTest extends TestCase
{
	use RefreshDatabase;

	/** @test */
	public function it_can_create_products()
	{
		$product = factory(Product::class)->raw();

		$this->post(route('api.v1.products.store'))->content($product);
	}
}
