<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ListProductTest extends TestCase
{
	use RefreshDatabase;

	/** @test */
	public function it_can_fetch_single_product()
	{
		$product = factory(Product::class)->create();

		$response = $this->getJson(route('api.v1.products.show', $product));

		$response->assertExactJson([
			'data' => [
				'type' => 'products',
				'id' => (string) $product->getRouteKey(),
				'attributes' => [
					'currency' => $product->currency,
					'description' => $product->description,
					'image' => $product->image,
					'price' => $product->price,
					'title' => $product->title,
					'images' => $product->images,
					'sizes' => $product->sizes
				],
				'links' => [
					'self' => route('api.v1.products.show', $product)
				]
			]
		]);
	}

	/** @test */
	public function it_can_fetch_all_products()
	{
		$products = factory(Product::class)->times(3)->create();

		$response = $this->getJson(route('api.v1.products.index'));

		$response->assertExactJson([
			'data' => [
				[
					'type' => 'products',
					'id' => (string) $products[0]->getRouteKey(),
					'attributes' => [
						'currency' => $products[0]->currency,
						'description' => $products[0]->description,
						'image' => $products[0]->image,
						'price' => $products[0]->price,
						'title' => $products[0]->title,
						'images' => $products[0]->images,
						'sizes' => $products[0]->sizes,
					],
					'links' => [
						'self' => route('api.v1.products.show', $products[0])
					]
				],
				[
					'type' => 'products',
					'id' => (string) $products[1]->getRouteKey(),
					'attributes' => [
						'currency' => $products[1]->currency,
						'description' => $products[1]->description,
						'image' => $products[1]->image,
						'price' => $products[1]->price,
						'title' => $products[1]->title,
						'images' => $products[1]->images,
						'sizes' => $products[1]->sizes,
					],
					'links' => [
						'self' => route('api.v1.products.show', $products[1])
					]
				],
				[
					'type' => 'products',
					'id' => (string) $products[2]->getRouteKey(),
					'attributes' => [
						'currency' => $products[2]->currency,
						'description' => $products[2]->description,
						'image' => $products[2]->image,
						'price' => $products[2]->price,
						'title' => $products[2]->title,
						'images' => $products[2]->images,
						'sizes' => $products[2]->sizes,
					],
					'links' => [
						'self' => route('api.v1.products.show', $products[2])
					]
				],
			],
			'links' => [
				'self' => route('api.v1.products.index')
			]
		]);
	}
}
