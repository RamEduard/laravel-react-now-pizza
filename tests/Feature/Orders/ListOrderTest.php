<?php

namespace Tests\Feature\Orders;

use App\Models\Order;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ListOrderTest extends TestCase
{
	use RefreshDatabase;

	/** @test */
	public function it_can_fetch_single_order()
	{
		$order = factory(Order::class)->create();

		$response = $this->getJson(route('api.v1.orders.show', $order));

		$response->assertExactJson([
			'data' => [
				'type' => 'orders',
				'id' => (string) $order->getRouteKey(),
				'attributes' => [
					'currency' => $order->currency,
					'subtotal' => $order->subtotal,
					'shipping' => $order->shipping,
					'total' => $order->total,
					'status' => $order->status,
					'addressDatas' => $order->addressDatas,
					'contactData' => $order->contactData,
					'products' => $order->products,
					'created_at' => $order->created_at
				],
				'links' => [
					'self' => route('api.v1.orders.show', $order)
				]
			]
		]);
	}

	/** @test */
	public function it_can_fetch_all_orders()
	{
		Sanctum::actingAs(
			factory(User::class)->create()
		);

		$orders = factory(Order::class)->times(3)->create();

		$response = $this->getJson(route('api.v1.orders.index'));

		$response->assertExactJson([
			'data' => [
				[
					'type' => 'orders',
					'id' => (string) $orders[0]->getRouteKey(),
					'attributes' => [
						'currency' => $orders[0]->currency,
						'subtotal' => $orders[0]->subtotal,
						'shipping' => $orders[0]->shipping,
						'total' =>    $orders[0]->total,
						'status' =>   $orders[0]->status,
						'addressDatas' => $orders[0]->addressDatas,
						'contactData' => $orders[0]->contactData,
						'products' => $orders[0]->products,
						'created_at' => $orders[0]->created_at
					],
					'links' => [
						'self' => route('api.v1.orders.show', $orders[0])
					]
				],
				[
					'type' => 'orders',
					'id' => (string) $orders[1]->getRouteKey(),
					'attributes' => [
						'currency' => $orders[1]->currency,
						'subtotal' => $orders[1]->subtotal,
						'shipping' => $orders[1]->shipping,
						'total' =>    $orders[1]->total,
						'status' =>   $orders[1]->status,
						'addressDatas' => $orders[1]->addressDatas,
						'contactData' => $orders[1]->contactData,
						'products' => $orders[1]->products,
						'created_at' => $orders[1]->created_at
					],
					'links' => [
						'self' => route('api.v1.orders.show', $orders[1])
					]
				],
				[
					'type' => 'orders',
					'id' => (string) $orders[2]->getRouteKey(),
					'attributes' => [
						'currency' => $orders[2]->currency,
						'subtotal' => $orders[2]->subtotal,
						'shipping' => $orders[2]->shipping,
						'total' =>    $orders[2]->total,
						'status' =>   $orders[2]->status,
						'addressDatas' => $orders[2]->addressDatas,
						'contactData' => $orders[2]->contactData,
						'products' => $orders[2]->products,
						'created_at' => $orders[2]->created_at
					],
					'links' => [
						'self' => route('api.v1.orders.show', $orders[2])
					]
				],
			],
			'links' => [
				'self' => route('api.v1.orders.index')
			]
		]);
	}
}
