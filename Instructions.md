Thank you very much for delivering your solution on time! It looks very good!

I'm going to describe some improvements that you should add in order to adequate your solution to innoscripta's expectations.

- You need to present one repository for backend and one repository for frontend as requested in the Pizza Task instructions.
- There are no currency change options. You need to add in the header of the page two buttons or a switcher to be able to select the preferred currency. You cannot give 2 prices, and the ordering process cannot be done with two prices.
- The cart should offer the possibility to add or remove items.
- Each field of the checkout form should have validation and should display a warning message in case invalid data is written or inserted.
- the orders history was not clean before I ordered.

I'm also sending you an attachment with more suggestions that could help you deliver a more correct solution. Check it out please.

## Exchange Rates

- https://exchangeratesapi.io/
- https://api.exchangeratesapi.io/latest
- https://fixer.io/
