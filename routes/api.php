<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
	return response()->json([
		'data' => [
			'messasge' => 'It is working.',
		],
	]);
});

// Auth and Users
Route::post('/login', 'Api\AuthController@login')->name('login');
Route::post('auth/login', 'Api\AuthController@login')->name('api.v1.auth.login');
Route::post('auth/register', 'Api\AuthController@register')->name('api.v1.auth.register');
Route::get('auth/logout', 'Api\AuthController@logout')->name('api.v1.auth.logout');
Route::middleware('auth:sanctum')->get('auth/current-user', 'Api\AuthController@currentUser')->name('api.v1.auth.current_user');

// Products
Route::get('products', 'Api\ProductController@index')->name('api.v1.products.index');
Route::post('products', 'Api\ProductController@store')->name('api.v1.products.store');
Route::get('products/search', 'Api\ProductController@search')->name('api.v1.products.search');
Route::get('products/{product}', 'Api\ProductController@show')->name('api.v1.products.show');
Route::put('products/{product}', 'Api\ProductController@update')->name('api.v1.products.update');
Route::delete('products/{product}', 'Api\ProductController@delete')->name('api.v1.products.delete');

// Orders
Route::middleware('auth:sanctum')->get('orders', 'Api\OrderController@index')->name('api.v1.orders.index');
Route::post('orders', 'Api\OrderController@store')->name('api.v1.orders.store');
Route::get('orders/{order}', 'Api\OrderController@show')->name('api.v1.orders.show');
Route::put('orders/{order}', 'Api\OrderController@update')->name('api.v1.orders.update');
Route::delete('orders/{order}', 'Api\OrderController@delete')->name('api.v1.orders.delete');

// HistoryExchangeRates
Route::get('history-exchange-rates', 'Api\HistoryExchangeRateController@index')->name('api.v1.history_exchange_rates.index');
Route::post('history-exchange-rates', 'Api\HistoryExchangeRateController@store')->name('api.v1.history_exchange_rates.store');
Route::get('history-exchange-rates/today', 'Api\HistoryExchangeRateController@today')->name('api.v1.history_exchange_rates.today');
Route::get('history-exchange-rates/{exchangeRate}', 'Api\HistoryExchangeRateController@show')->name('api.v1.history_exchange_rates.show');
Route::put('history-exchange-rates/{exchangeRate}', 'Api\HistoryExchangeRateController@update')->name('api.v1.history_exchange_rates.update');
Route::delete('history-exchange-rates/{exchangeRate}', 'Api\HistoryExchangeRateController@delete')->name('api.v1.history_exchange_rates.delete');

Route::get('users/{user}', 'Api\UserController@show')->name('api.v1.users.show');

Route::fallback(function (Request $request) {
	return response()->json(['message' => 'Not Found.'], 404);
})->name('api.v1.error.404');
