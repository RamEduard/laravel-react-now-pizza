<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ResourceCollection;
use App\Http\Resources\ResourceObject;
use App\Models\HistoryExchangeRate;
use App\Utils\ExchangeRatesApiService;
use Illuminate\Http\Request;

class HistoryExchangeRateController extends Controller
{

	public function index()
	{
		return ResourceCollection::make(HistoryExchangeRate::all());
	}

	public function today()
	{
		$rates = ExchangeRatesApiService::getRates();

		return ResourceObject::make($rates);
	}

  public function show(HistoryExchangeRate $exchangeRate)
	{
		return ResourceObject::make($exchangeRate);
	}

	public function store(Request $request)
	{
		$exchangeRate = HistoryExchangeRate::create($request->all());

		return ResourceObject::make($exchangeRate);
	}

	public function update(Request $request, HistoryExchangeRate $exchangeRate)
	{
		$exchangeRate->update($request->all());

		return ResourceObject::make($exchangeRate);
	}

	public function delete(HistoryExchangeRate $exchangeRate)
	{
		$exchangeRate->delete();

		return response()->json([
			'data' => null
		], 204);
	}
}
