<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\User;
use Exception;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

	public function register(Request $request)
	{
		$this->validator($request->all())->validate();

		$data = $request->all();

		$user = User::create([
			'name' => $data['name'],
			'email' => $data['email'],
			'password' => Hash::make($data['password']),
		]);

		$this->guard()->login($user);

		return response()->json([
			'data' => [
				'user' => $request->user(),
				'self' => [
					'link' => route('api.v1.auth.register')
				]
			]
		]);
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'name' => ['required', 'string', 'max:255'],
			'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
			//'password' => ['required', 'string', 'min:4', 'confirmed'],
			// NO PASSWORD CONFIRMATION
			'password' => ['required', 'string', 'min:4'],
		]);
	}

	protected function guard()
	{
		return Auth::guard();
	}

	/**
	 * Handle an authentication attempt.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return Response
	 */
	public function login(Request $request)
	{
		$credentials = $request->only('email', 'password');

		if (Auth::attempt($credentials)) {
			// Authentication passed...
			return response()->json([
				'data' => [
					'user' => $request->user(),
					'self' => [
						'link' => route('api.v1.auth.login')
					]
				]
			]);
		}

		return response()->json([
			'data' => null,
			'error' => 'Bad credentials',
		], 401);
	}

	/**
	 * Handle the logout.
	 *
	 * @return Response
	 */
	public function logout()
	{
		Auth::logout();
		return response()->json([
			'data' => [
				'message' => 'Logged Out'
			]
		], 200);
	}

	public function currentUser(Request $request) {
		try {
			return response()->json([
				'data' => [
					'user' => $request->user(),
					'self' => [
						'link' => route('api.v1.auth.current_user')
					]
				]
			]);
		} catch (Exception $exception) {
			return response()->json([
				'error' => $exception->getMessage()
			]);
		}
	}
}
