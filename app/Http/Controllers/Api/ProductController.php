<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ResourceCollection;
use App\Http\Resources\ResourceObject;
use App\Models\Product;
use App\Utils\ExchangeRatesApiService;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductController extends Controller
{

	public function index(Request $request)
	{
		$currency = $request->query('currency');

		$products = Product::all();

		if (isset($currency) && $currency !== 'eur') {
			// Get rates
			$exchangeRate = ExchangeRatesApiService::getRates('EUR', Str::upper($currency));

			foreach ($products as $product) {
				if ($product->currency !== $currency) {
					$product->currency = $currency;
					$product->price = ($product->price * $exchangeRate->rate);
				}
			}
		}

		return ResourceCollection::make($products);
	}


  public function show(Product $product)
	{
		return ResourceObject::make($product);
	}

	public function store(Request $request)
	{
		$product = Product::create($request->all());

		return ResourceObject::make($product);
	}

	public function update(Request $request, Product $product)
	{
		$product->update($request->all());

		return ResourceObject::make($product);
	}

	public function delete(Product $product)
	{
		$product->delete();

		return response()->json([
			'data' => null
		], 204);
	}

	public function search(Request $request)
	{
		$querySearch = $request->query('q');
		$currency = $request->query('currency');

		$products = Product::where('title', 'LIKE', "%{$querySearch}%")
			->orWhere('description', 'LIKE', "%{$querySearch}%")
			->get();

		if (isset($currency) && $currency !== 'eur') {
			// Get rates
			$exchangeRate = ExchangeRatesApiService::getRates('EUR', Str::upper($currency));

			foreach ($products as $product) {
				if ($product->currency !== $currency) {
					$product->currency = $currency;
					$product->price = ($product->price * $exchangeRate->rate);
				}
			}
		}

		return ResourceCollection::make($products);
	}
}
