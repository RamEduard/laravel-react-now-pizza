<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ResourceCollection;
use App\Http\Resources\ResourceObject;
use App\Models\Order;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class OrderController extends Controller
{
  public function index(Request $request)
	{
		$user = $request->user();

		$orders = Order::whereHas('contactData', function(Builder $query) use($user) {
			$query->where('email', '=', $user->attributes()['email']);
		})->get();

		return ResourceCollection::make($orders);
	}

  public function show(Order $order)
	{
		return ResourceObject::make($order);
	}

	public function store(Request $request)
	{
		$data = $request->all();

		$order = Order::create([
			'currency' => $data['currency'],
			'shipping' => $data['shipping'],
			'subtotal' => $data['subtotal'],
			'total' =>    $data['total'],
			'status' =>   $data['status']
		]);

		if (isset($data['addressDatas']) && is_array($data['addressDatas'])) {
			// create addressData objects
			foreach ($data['addressDatas'] as $addressData) {
				$order->addressDatas()->create($addressData);
			}
		}

		if (isset($data['contactData'])) {
			// create contactData objects
			$order->contactData()->create($data['contactData']);
		}

		if (isset($data['products']) && is_array($data['products'])) {
			// create product objects
			foreach ($data['products'] as $product) {
				$order->products()->attach($product['id'], [
					'count' => $product['count'],
					'size' => $product['size'],
					'price' => $product['price'],
				]);
			}
		}

		$order->save();
		$order->refresh();

		return ResourceObject::make($order);
	}

	public function update(Request $request, Order $order)
	{
		$order->update($request->all());

		return ResourceObject::make($order);
	}

	public function delete(Order $order)
	{
		$order->delete();

		return response()->json([
			'data' => null
		], 204);
	}
}
