<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	public $type = 'products';

	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'price' => 'decimal:2',
	];

	public function attributes()
	{
		return [
			'currency' => $this->currency,
			'description' => $this->description,
			'image' => $this->image,
			'price' => $this->price,
			'title' => $this->title,
			'images' => $this->images,
			'sizes' => $this->sizes
		];
	}

	public function orders()
	{
		return $this->belongsToMany(\App\Models\Order::class)->using(\App\Models\OrderProduct::class);
	}

	public function images()
	{
		return $this->hasMany(\App\Models\ProductImage::class);
	}

	public function sizes()
	{
		return $this->hasMany(\App\Models\ProductSize::class);
	}
}
