<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HistoryExchangeRate extends Model
{
	/**
	 * Type
	 *
	 * @var string
	 */
	public $type = 'history_exchange_rates';

	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'rate' => 'float',
	];

	public function attributes()
	{
		return [
			'from' => $this->from,
			'to' => $this->to,
			'rate' => $this->rate,
			'api_provider' => $this->api_provider,
			'created_at' => $this->created_at,
		];
	}
}
