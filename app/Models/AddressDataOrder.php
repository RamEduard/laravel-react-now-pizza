<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class AddressDataOrder extends Pivot
{
	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'address_data_id' => 'integer',
		'order_id' => 'integer',
	];

	public function order()
	{
		return $this->belongsTo(\App\Models\Order::class);
	}

	public function addressData()
	{
		return $this->belongsTo(\App\Models\AddressData::class);
	}
}
