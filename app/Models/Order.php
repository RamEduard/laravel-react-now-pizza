<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	/**
	 * Type
	 *
	 * @var string
	 */
	public $type = 'orders';

	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'subtotal' => 'float',
		'shipping' => 'float',
		'total' => 'float',
	];

	public function attributes()
	{
		return [
			'currency' => $this->currency,
			'subtotal' => $this->subtotal,
			'shipping' => $this->shipping,
			'total' => $this->total,
			'status' => $this->status,
			'addressDatas' => $this->addressDatas,
			'contactData' => $this->contactData,
			'products' => $this->products,
			'created_at' => $this->created_at
		];
	}

	public function products()
	{
		return $this->belongsToMany(\App\Models\Product::class)->using(\App\Models\OrderProduct::class)->withPivot(['count', 'price', 'size']);
	}

	public function addressDatas()
	{
		return $this->belongsToMany(\App\Models\AddressData::class)->using(\App\Models\AddressDataOrder::class);
	}

	public function contactData()
	{
		return $this->hasOne(\App\Models\ContactData::class);
	}

	public function user() {
		return $this->belongsTo(\App\User::class);
	}
}
