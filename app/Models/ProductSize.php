<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSize extends Model
{
	/**
	 * Type
	 *
	 * @var string
	 */
	public $type = 'product_images';

	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = [];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'product_id' => 'integer',
		'price' => 'decimal:2',
	];


	public function product()
	{
		return $this->belongsTo(\App\Models\Product::class);
	}
}
