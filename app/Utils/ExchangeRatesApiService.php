<?php

namespace App\Utils;

use App\Models\HistoryExchangeRate;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Http;

class ExchangeRatesApiService {

	static $apiUrl = 'https://api.exchangeratesapi.io/latest';

	/**
	 * Get rates for today, if exists return from db
	 *
	 * @param  string $from
	 * @param  string $to
	 * @return HistoryExchangeRate
	 */
	public static function getRates($from = 'EUR', $to = 'USD')
	{
		$exchangeRate = HistoryExchangeRate::where('from', $from)
			->where('to', $to)
			->whereDate('created_at', Carbon::today())
			->first();

		if (is_null($exchangeRate)) {
			// Create new one
			$response = Http::get(self::$apiUrl . "?symbols=$to");

			$ratesJson = $response->json();

			if (!isset($ratesJson['rates'])) {
				throw new \Exception('Service exchangeratesapi.io could not return rates.');
			}

			$exchangeRate = HistoryExchangeRate::create([
				'from' => $from,
				'to' => $to,
				'rate' => $ratesJson['rates'][$to],
				'api_provider' => 'exchangeratesapi.io'
			]);
		}

		return $exchangeRate;
	}
}
