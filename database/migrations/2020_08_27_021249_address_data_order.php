<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddressDataOrder extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('address_data_order', function (Blueprint $table) {
			$table->id();
			$table->foreignId('address_data_id')->constrained('address_data')->cascadeOnDelete();
			$table->foreignId('order_id')->constrained('orders')->cascadeOnDelete();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('address_data_order');
	}
}
