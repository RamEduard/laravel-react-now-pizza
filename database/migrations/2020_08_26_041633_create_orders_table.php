<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
						$table->id();
						$table->enum('currency', ['eur', 'usd'])->default('eur');
						$table->decimal('subtotal', 8, 2);
            $table->decimal('shipping', 8, 2);
						$table->decimal('total', 8, 2);
            $table->enum('status', ["pending","pickup","delivering","successful","failed","cancelled"])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
