<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ContactData;
use Faker\Generator as Faker;

$factory->define(ContactData::class, function (Faker $faker) {
    return [
        'email' => $faker->safeEmail,
        'phone' => $faker->phoneNumber,
        'order_id' => factory(\App\Models\Order::class),
    ];
});
