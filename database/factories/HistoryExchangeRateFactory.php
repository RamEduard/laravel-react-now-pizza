<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\HistoryExchangeRate;
use Faker\Generator as Faker;

$factory->define(HistoryExchangeRate::class, function (Faker $faker) {
    return [
        'base' => $faker->word,
        'rate' => $faker->randomFloat(2, 0, 999.99),
        'api_provider' => $faker->word,
    ];
});
