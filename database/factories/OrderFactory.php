<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
	return [
		'currency' => $faker->randomElement(['eur', 'usd']),
		'subtotal' => $faker->randomFloat(2, 0, 999999.99),
		'shipping' => $faker->randomFloat(2, 0, 999999.99),
		'total' => $faker->randomFloat(2, 0, 999999.99),
		'status' => $faker->randomElement(["pending", "pickup", "delivering", "successful", "failed", "cancelled"]),
		'user_id' => factory(\App\User::class),
	];
});
