<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ProductSize;
use Faker\Generator as Faker;

$factory->define(ProductSize::class, function (Faker $faker) {
    return [
        'product_id' => factory(\App\Models\Product::class),
        'size' => $faker->word,
        'price' => $faker->randomFloat(2, 0, 999999.99),
    ];
});
