<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\AddressDataOrder;
use Faker\Generator as Faker;

$factory->define(AddressDataOrder::class, function (Faker $faker) {
    return [
        'order_id' => factory(\App\Models\Order::class),
        'address_data_id' => factory(\App\Models\AddressData::class),
    ];
});
