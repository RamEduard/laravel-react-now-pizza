<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OrderProduct;
use Faker\Generator as Faker;

$factory->define(OrderProduct::class, function (Faker $faker) {
    return [
				'count' => $faker->randomNumber(),
				'price' => $faker->randomFloat(2, 0, 999999.99),
				'size' => $faker->randomElement(['small', 'medium', 'large']),
        'order_id' => factory(\App\Models\Order::class),
        'product_id' => factory(\App\Models\Product::class),
    ];
});
