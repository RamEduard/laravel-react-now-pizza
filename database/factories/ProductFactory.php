<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
	return [
		'currency' => $faker->randomElement(['eur', 'usd']),
		'title' => $faker->sentence(4),
		'description' => $faker->text(150),
		'image' => $faker->text,
		'price' => $faker->randomFloat(2, 0, 999999.99),
	];
});
