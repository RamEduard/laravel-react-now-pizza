<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\AddressData;
use Faker\Generator as Faker;

$factory->define(AddressData::class, function (Faker $faker) {
    return [
        'type' => $faker->randomElement(["billing","shipping","both"]),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'address1' => $faker->streetAddress,
        'address2' => $faker->secondaryAddress,
        'country' => $faker->country,
        'city' => $faker->city,
        'state' => $faker->word,
        'zip' => $faker->postcode,
        'order_id' => factory(\App\Models\Order::class),
    ];
});
