<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('users')->insert([
			'name' => 'Ramon Serrano',
			'email' => 'ramon.calle.88@gmail.com',
			'password' => Hash::make('q1w2e3r4')
		]);
	}
}
