<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSizeSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$sizes = ['small', 'medium', 'large'];

		for ($i = 0; $i < 8; $i++) {
			// Small
			DB::table('product_sizes')->insert([
				'price' => rand(2, 5),
				'size' => $sizes[0],
				'product_id' => ($i + 1)
			]);

			// Medium
			DB::table('product_sizes')->insert([
				'price' => rand(5, 10),
				'size' => $sizes[1],
				'product_id' => ($i + 1)
			]);

			// Large
			DB::table('product_sizes')->insert([
				'price' => rand(10, 15),
				'size' => $sizes[2],
				'product_id' => ($i + 1)
			]);
		}
	}
}
