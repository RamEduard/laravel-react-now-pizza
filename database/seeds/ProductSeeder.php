<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('products')->insert([
			'id' => 1,
			'currency' => 'eur',
			'title' => 'Caprese',
			'description' => 'A classic pizza with tomatoes, mozzarella, fresh basil leaves and balsamic vinegar.',
			'image' => asset('/img/caprese/1.jpg'),
			'price' => 3
		]);

		DB::table('products')->insert([
			'id' => 2,
			'currency' => 'eur',
			'title' => 'Calzone',
			'description' => 'A calzone with onion, green bell pepper, pepperoni, spinach and mozzarella cheese.',
			'image' => asset('/img/calzone/1.jpg'),
			'price' => 3
		]);

		DB::table('products')->insert([
			'id' => 3,
			'currency' => 'eur',
			'title' => 'Napoletana',
			'description' => 'This pizza should have minimal toppings: San Marzano tomato sauce, buffalo mozzarella cheese, and basil.',
			'image' => asset('/img/pizza-3_640.jpg'),
			'price' => 3
		]);

		DB::table('products')->insert([
			'id' => 4,
			'currency' => 'eur',
			'title' => 'California Style',
			'description' => 'A pizza with ricotta, red peppers, mustard, and pate',
			'image' => asset('/img/california/1.jpg'),
			'price' => 3
		]);

		DB::table('products')->insert([
			'id' => 5,
			'currency' => 'eur',
			'title' => 'Cheese',
			'description' => 'A pizza for cheese lovers with tasty cheese, mazzarella and parmesan grated.',
			'image' => asset('/img/cheese/1.jpg'),
			'price' => 3
		]);

		DB::table('products')->insert([
			'id' => 6,
			'currency' => 'eur',
			'title' => 'Pepperoni',
			'description' => 'You literally can\'t go wrong with pepperoni and mozzarella cheese. Classic for a reason.',
			'image' => asset('/img/pepperoni/1.jpg'),
			'price' => 3
		]);

		DB::table('products')->insert([
			'id' => 7,
			'currency' => 'eur',
			'title' => 'Hawaiian',
			'description' => 'A pizza with mozzarella cheese, cooked ham, 3 slices bacon and pineapple.',
			'image' => asset('/img/hawaiian/1.jpg'),
			'price' => 3
		]);

		DB::table('products')->insert([
			'id' => 8,
			'currency' => 'eur',
			'title' => 'Veggie Lover\'s Pizza',
			'description' => 'It is officially a vegetable. This garden delight has all the fresh veggie toppings you love: mushrooms, red onions, green bell peppers, Roma tomatoes and black olives.',
			'image' => asset('/img/veggie/1.jpg'),
			'price' => 3
		]);
	}
}
